# Iceland Details


## Flights

```
Booking reference
*4DWDL2*

Outbound 2024-02-19
*Toronto (YYZ) - Keflavik (KEF)*
19:45 - 06:25 +1 (overnight flight)
Icelandair FI602
5h 40m
Confirmed
From Terminal 3
Economy Flex

Return 2024-12-29
*Keflavik (KEF) - Toronto (YYZ)*
17:00 - 18:15
Icelandair FI603
To Terminal 3
6h 15m
Confirmed
Economy Flex
```

## Toronto Hotel 2024-12-29

```
Confirmation #91005902
Hampton Inn by Hilton Toronto Airport Corporate Centre
5515 Eglinton Avenue West
Toronto ON M9C 5K5 CA
+14166463000
Your Room Information
Guest Name: Dana Wardlaw
Guests: 4 Adults
Rooms: 1
Room Plan: 2 QUEEN BEDS NONSMOKING
Your Rate Information AAA MEMBER RATE
------------------------------
Rate per night
29-Dec-2024 - 30-Dec-2024 231.69 CAD
Total for Stay per Room Rate 231.69 CAD
Taxes 45.83 CAD
------------------------------
Total price for Stay 277.52 CAD
```