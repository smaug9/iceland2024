# Iceland Itinerary

## 2024-12-19 Thu: Ith > Buff > Toronto

* 1000a dep ith > buffalo (3h drive)
*  100p arr buffalo, pick up eli
*  200p dep buff > toronto (2h drive)
*  400p arr toronto airport, yyz
  * park car in long term parking
*  745p dep yyz > kef (reykjavik, 6h flight)

## 2024-12-20 Fri: Arrive Iceland

*  700a arr kef
*  800a taxi to airbnb (laugavegur 15, host arni)
*  900a drop off bags at airbnb
*  200p check in airbnb

## 2024-12-21 Sat: Reykjavik

* explore reykjavik
* laugavegur 15

## 2024-12-22 Sun: Reykjavik

* explore reykjavik
* laugavegur 15

## 2024-12-23 Mon: South Coast Tour Day 1

* 800a checkin for tour (3 day south coast tour)

## 2024-12-24 Tue: Xmas Eve: South Coast Tour Day 2

* 3 day south coast tour

## 2024-12-25 Wed: Xmas Day: South Coast Tour Day 3

* 3 day south coast tour
* evening: reykjavik

## 2024-12-26 Thu: West Iceland Tour Day 1

* 800a checkin for tour (2 day snaefellsnses/west iceland)

## 2024-12-27 Fri: West Iceland Tour Day 2

* 2 day snaefellsnses/west iceland
* evening: reykjavik
* 200p checkin Sorlaskjol

## 2024-12-28 Sat: Reykjavik

* explore reykjavik

## 2024-12-29 Sun: Reykjavik > Toronto

* 1200p checkout sorlaskjol
*  500p dep kef
*  615p arr yyz
*  700p taxi airport > hotel (Toronto Airport Hampton Inn)
*  730p arr hotel toronto

## 2024-12-30 Mon: Toronto > Buff > Ith

*  800a dep tor > buffalo
* 1000a dep buff > ith
*  100p arr ith

## 2024-12-31 Tue: New Years Eve

* celebrate new years ithaca



  